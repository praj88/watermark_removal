import imageio
#imageio.plugins.ffmpeg.download()

import moviepy.editor as mpe
import numpy as np
import scipy
from sklearn import decomposition

%matplotlib inline
import matplotlib.pyplot as plt
import cv2
from PIL import Image
from numpy.linalg import svd
from moviepy.video.io.bindings import mplfig_to_npimage


from scipy import sparse
from  sklearn.utils.extmath import randomized_svd
import fbpca
#------------------------------------------------------------- Helper Functions ----------------------------------------------
def create_data_matrix_from_video(clip, fps, scale, channel):
    print(channel)
    return np.vstack([scipy.misc.imresize(image_recons(clip.get_frame(i/float(fps)),channel).astype(int),scale).flatten() for i in range(fps * int(clip.duration))]).T

# convert rgb to gray
def rgb2gray(rgb):
    return np.dot(rgb[...,:3], [0.299, 0.587, 0.114])

# convert rgb to r,b,g channel
def image_recons(rgb, channel):
    b,g,r = cv2.split(rgb)
    if channel == 'r':

        return r


    elif channel == 'g':

        return g

    else:

        return b


# Rescale image array to 0-255
def rescale(arr):
    arr_min = arr.min()
    arr_max = arr.max()
    return (arr - arr_min) / (arr_max - arr_min)

# Generate output video
def make_video(r_mat_reshaped, g_mat_reshaped,  b_mat_reshaped, dims, fps, filename):

    i = 0
    height = dims[0]
    width = dims[1]
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'mp4v') # Be sure to use lower case
    out = cv2.VideoWriter(filename, fourcc, fps, (596,336))


    r_mat_reshaped = np.reshape(r_No_WM, (dims[0], dims[1], -1))
    g_mat_reshaped = np.reshape(g_No_WM, (dims[0], dims[1], -1))
    b_mat_reshaped = np.reshape(b_No_WM, (dims[0], dims[1], -1))
    print(r_mat_reshaped.shape)

    while i < (r_mat_reshaped[1,1,:].size):
        merged_image = cv2.merge((r_mat_reshaped[:,:,i],g_mat_reshaped[:,:,i],b_mat_reshaped[:,:,i]))
        print(merged_image.shape)
        merged_image_rescaled = (255.0 *rescale(merged_image)).astype(dtype='uint8')
        print(type(merged_image_rescaled))
        print(type(merged_image_rescaled.shape))
        cv2.imshow('video',merged_image_rescaled)
        if (cv2.waitKey(1) & 0xFF) == ord('q'): # Hit `q` to exit
            break
        print("{} out of {}".format(i, r_mat_reshaped[1,1,:].size))

        #img = Image.fromarray(merged_image_rescaled.astype(dtype='uint8'),'RGB')
        #frame = cv2.imread(merged_image_rescaled)
        out.write(merged_image_rescaled)
        i += 1


    cv2.destroyAllWindows()
    out.release()
    print("The output video is {}".format(filename))


def resize_video(video_path, out_file):

    cap = cv2.VideoCapture(video_path)
    width = int(cap.get(3))
    height = int(cap.get(4))
    fps = cap.get(cv2.CAP_PROP_FPS)
    print(fps)
    # Define the codec and create VideoWriter object
    fourcc = cv2.VideoWriter_fourcc(*'XVID')
    out = cv2.VideoWriter(out_file, fourcc, fps, (596,336))

    while(cap.isOpened()):
        ret, frame = cap.read()
        if ret==True:
            frame = cv2.resize(frame, (596,336))

            # write the flipped frame
            out.write(frame)

            cv2.imshow('frame',frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        else:
            break

    # Release everything if job is finished
    cap.release()
    out.release()
    cv2.destroyAllWindows()
#------------------------------------------------------------- Robust PCA Helper Functions ----------------------------------------------
def converged(Z, d_norm):
    err = np.linalg.norm(Z, 'fro') / d_norm
    print('error: ', err)
    return err < TOL

def shrink(M, tau):
    S = np.abs(M) - tau
    return np.sign(M) * np.where(S>0, S, 0)


def _svd(M, rank): return fbpca.pca(M, k=min(rank, np.min(M.shape)), raw=True)

def norm_op(M): return _svd(M, 1)[1][0]


def svd_reconstruct(M, rank, min_sv):
    u, s, v = _svd(M, rank)
    s -= min_sv
    nnz = (s > 0).sum()
    return u[:,:nnz] @ np.diag(s[:nnz]) @ v[:nnz], nnz

def pcp(X, maxiter=10, k=10): # refactored
    m, n = X.shape
    trans = m<n
    if trans: X = X.T; m, n = X.shape

    lamda = 1/np.sqrt(m)
    op_norm = norm_op(X)
    Y = np.copy(X) / max(op_norm, np.linalg.norm( X, np.inf) / lamda)
    mu = k*1.25/op_norm; mu_bar = mu * 1e7; rho = k * 1.5

    d_norm = np.linalg.norm(X, 'fro')
    L = np.zeros_like(X); sv = 1

    examples = []

    for i in range(maxiter):
        print("rank sv:", sv)
        X2 = X + Y/mu

        # update estimate of Sparse Matrix by "shrinking/truncating": original - low-rank
        S = shrink(X2 - L, lamda/mu)

        # update estimate of Low-rank Matrix by doing truncated SVD of rank sv & reconstructing.
        # count of singular values > 1/mu is returned as svp
        L, svp = svd_reconstruct(X2 - S, sv, 1/mu)

        # If svp < sv, you are already calculating enough singular values.
        # If not, add 20% (in this case 240) to sv
        sv = svp + (1 if svp < sv else round(0.05*n))

        # residual
        Z = X - L - S
        Y += mu*Z; mu *= rho

        examples.extend([S[140,:], L[140,:]])

        if m > mu_bar: m = mu_bar
        if converged(Z, d_norm): break

    if trans: L=L.T; S=S.T
    return L, S, examples

#------------------------------------------------------------- Main Function to remove watermark using SVD ----------------------------------------------
def remove_wm_svd(video_path):
    np.set_printoptions(precision=4, suppress=True)
    video = mpe.VideoFileClip(video_path)
    duration = (video.duration)

    # get width and height of the frame
    vcap = cv2.VideoCapture(video_path)
    width = int(vcap.get(3))
    height = int(vcap.get(4))

    # PARAMETERS`
    scale = 100 # Adjust scale to change resolution of image
    dims = (int(height), int(width))
    fps = int(vcap.get(cv2.CAP_PROP_FPS)) #30      # frames per second)

    M_r = create_data_matrix_from_video(video.subclip(0,duration), fps, scale, 'r')
    M_g = create_data_matrix_from_video(video.subclip(0,duration), fps, scale, 'g')
    M_b = create_data_matrix_from_video(video.subclip(0,duration), fps, scale, 'b')

    print(dims, M_r.shape)

    Ur, Sr, Vr =  np.linalg.svd(M_r, full_matrices=False) #decomposition.randomized_svd(M_r, 2)
    Ug, Sg, Vg = np.linalg.svd(M_g, full_matrices=False) #
    Ub, Sb, Vb = np.linalg.svd(M_g, full_matrices=False) #

    print(Ur.shape)
    print(Sr.shape)
    print(Vr.shape)

    low_rank_r = np.expand_dims(Ur[:,0], 1) * Sr[0] * np.expand_dims(Vr[0,:], 0)
    low_rank_g = np.expand_dims(Ug[:,0], 1) * Sg[0] * np.expand_dims(Vg[0,:], 0)
    low_rank_b = np.expand_dims(Ub[:,0], 1) * Sb[0] * np.expand_dims(Vb[0,:], 0)

    print(low_rank_r.shape)

    # Rescale the Matrix to be between 0 and 255
    r_No_WM = 255.0 * rescale(M_r - low_rank_r)
    g_No_WM = 255.0 * rescale(M_g - low_rank_g)
    b_No_WM = 255.0 * rescale(M_b - low_rank_b)

    r_No_WM.shape

    r_mat_reshaped = np.reshape(r_No_WM, (dims[0], dims[1], -1))
    g_mat_reshaped = np.reshape(g_No_WM, (dims[0], dims[1], -1))
    b_mat_reshaped = np.reshape(b_No_WM, (dims[0], dims[1], -1))

    r_mat_reshaped.shape
    type(r_mat_reshaped[:,:,1])

    # Generate video file from matrix
    make_video(r_mat_reshaped, g_mat_reshaped, b_mat_reshaped, dims, fps, "/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/Dog/out_dog2.mp4")
    #mpe.VideoFileClip("/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/Dog/figures2.mp4").subclip(0,7).ipython_display(width=300)


#------------------------------------------------------------- Main Function to remove watermark using Robust PCA ----------------------------------------------
def remove_wm_PCA(video_path):
    TOL=1e-9
    MAX_ITERS=3
    np.set_printoptions(precision=4, suppress=True)
    video = mpe.VideoFileClip(video_path) #video_path)
    vcap = cv2.VideoCapture(video_path)
    duration = int(video.duration)
    print(duration)
    print(video.size)

    # PARAMETERS`
    scale = 100 # Adjust scale to change resolution of image
    dims = (int(336), int(596))
    fps = int(vcap.get(cv2.CAP_PROP_FPS))      # frames per second

    # Create Matrix for R, G and B
    M_r = create_data_matrix_from_video(video.subclip(0,duration), fps, scale, 'r')
    print(dims, M_r.shape)

    M_g = create_data_matrix_from_video(video.subclip(0,duration), fps, scale, 'g')
    print(dims, M_g.shape)

    M_b = create_data_matrix_from_video(video.subclip(0,duration), fps, scale, 'b')
    print(dims, M_b.shape)

    # Get Low Rank decomposition for Matrix  R, G, B
    Lr, Sr, examples_r =  pcp(M_r, maxiter=10, k=5)
    Lg, Sg, examples_g =  pcp(M_g, maxiter=10, k=5)
    Lb, Sb, examples_b =  pcp(M_b, maxiter=10, k=5)

    print(Lr.shape)
    print(Sr.shape)

    # Rescale the Matrix to be between 0 and 255
    r_No_WM = 255.0 * rescale(M_r - Lr)
    g_No_WM = 255.0 * rescale(M_g - Lg)
    b_No_WM = 255.0 * rescale(M_b - Lb)

    r_No_WM.shape
    r_mat_reshaped = np.reshape(r_No_WM, (dims[0], dims[1], -1))
    g_mat_reshaped = np.reshape(g_No_WM, (dims[0], dims[1], -1))
    b_mat_reshaped = np.reshape(b_No_WM, (dims[0], dims[1], -1))

    r_mat_reshaped.shape
    type(r_mat_reshaped[:,:,1])

    # Generate video file from matrix
    make_video(r_mat_reshaped, g_mat_reshaped, b_mat_reshaped, dims, fps, "/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/Dog/out_dog2_PCA_1.mp4")
    #mpe.VideoFileClip("/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/Dog/figures2.mp4").subclip(0,7).ipython_display(width=300)



#------------------------------------------------------------- Run Main Function ----------------------------------------------

'''Re size the video into 336* 596 prior to perforing PCA or SVD'''

video_path = "/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/WR_SVD_PCA/Original/dog2.mp4"
out_file = '/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/WR_SVD_PCA/Resized Input/dog2_resized.avi'
resize_video(video_path, out_file)

'''Perform SVD or PCA'''

remove_wm_svd("/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/WR_SVD_PCA/Resized Input/dog2.mp4")
remove_wm_PCA("/Users/prajwalshreyas/Desktop/Singularity/WatermarkRemoval/WR_SVD_PCA/Resized Input/dog2.mp4")
